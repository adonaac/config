set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/vundle'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'tomtom/tcomment_vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'kien/ctrlp.vim'
Plugin 'nanotech/jellybeans.vim'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'wavded/vim-stylus'

call vundle#end()
filetype plugin indent on

set history=1000

let mapleader = ","
let g:mapleader = ","
let g:EasyMotion_do_mapping = 0
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-tab>"
let g:UltiSnipsEditSplit="vertical"
let g:NERDTreeCopyCmd="cp -r"

nmap <leader>w :w<CR>
nmap <leader>q :q<CR>
nmap <leader>f :CtrlP<CR>
nmap s <Plug>(easymotion-bd-w)

set autoread

set wildmenu
set wildignore=*.o,*~
set ruler
set number
set hid
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set ignorecase
set smartcase
set incsearch
set magic
set showmatch
set mat=2
set autochdir
set foldmethod=manual
set guicursor=a:block-Cursor/lCursor
set guicursor+=a:blinkon0
set relativenumber
syntax enable
colorscheme jellybeans 
if has('win32')
    set guifont=Inconsolata:h12
elseif has('unix')
    set guifont=Inconsolata\ 12
endif
set guioptions=e
set encoding=utf8
set nobackup
set nowb
set noswapfile

set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set softtabstop=4
set ai
set si
set wrap

map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

nnoremap <silent> <F2> :NERDTree<CR>

cd ~/dev
autocmd VimEnter * NERDTree
autocmd VimEnter * IndentGuidesEnable
