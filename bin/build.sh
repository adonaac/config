zip -9 -q -r temp.love .
cat "C:/Program Files/LOVE/love.exe" temp.love > game.exe
mkdir release
rm temp.love
mv game.exe release/game.exe
cd release
cp "C:/Program Files/LOVE/SDL2.dll" .
cp "C:/Program Files/LOVE/OpenAL32.dll" .
cp "C:/Program Files/LOVE/DevIL.dll" .
cp "C:/Program Files/LOVE/love.dll" .
cp "C:/Program Files/LOVE/lua51.dll" .
cp "C:/Program Files/LOVE/mpg123.dll" .
cp "C:/Program Files/LOVE/msvcp120.dll" .
cp "C:/Program Files/LOVE/msvcr120.dll" .
cd ..
zip -9 -q -r game.zip release
rm -R release
